package fr.ludo1520.philu;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class SquareGen extends BaseUSBlink {

    EditText mEdPrescaler, mEdPeriod, mEdPulse;
    public static int mPrescaler; // 0..65535
    public static long mPeriod, mPulse; // need 4 octets unsigned
    public static byte mTimerRunning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_square_gen);
        mEdPrescaler = (EditText)findViewById(R.id.edPrescaler);
        mEdPeriod = (EditText)findViewById(R.id.edPeriod);
        mEdPulse = (EditText)findViewById(R.id.edPulse);

        MainNav.mOutBuffer[0]=1; // ask for information
        MainNav.mOutBuffer[1]=10; // get square gen. parameters
        SendData();
    }

    static void GetTimerParam() {

        Log.d("phiLu", "SQUARE GEN : GetTimerParam : mBuff[2]=" + MainNav.mBuffer[2] + " mBuff[3]=" + MainNav.mBuffer[3]+ "\n");
        Log.d("phiLu", "SQUARE GEN : unsigned : mBuff[2]=" + MainNav.GetUnsignBuff(2) + " mBuff[3]=" + MainNav.GetUnsignBuff(3)+ "\n");

/*
        mPrescaler = (char)(MainNav.mBuffer[2] + (((char)MainNav.mBuffer[3])<<8));
        mPeriod = MainNav.mBuffer[4] + (((long)MainNav.mBuffer[5])<<8)
                    + (((long)MainNav.mBuffer[6])<<16)
                    + (((long)MainNav.mBuffer[7])<<24);
        mPulse =  MainNav.mBuffer[8] + (((long)MainNav.mBuffer[9])<<8)
                    + (((long)MainNav.mBuffer[10])<<16)
                    + (((long)MainNav.mBuffer[11])<<24);
        mTimerRunning = MainNav.mBuffer[12];*/

        /*
        mPrescaler = (char)(MainNav.GetUnsignBuff(2) + (((char)MainNav.GetUnsignBuff(3))<<8));
        mPeriod = MainNav.GetUnsignBuff(4) + (((long)MainNav.GetUnsignBuff(5))<<8)
                + (((long)MainNav.GetUnsignBuff(6))<<16)
                + (((long)MainNav.GetUnsignBuff(7))<<24);
        mPulse =  MainNav.GetUnsignBuff(8) + (((long)MainNav.GetUnsignBuff(9))<<8)
                + (((long)MainNav.GetUnsignBuff(10))<<16)
                + (((long)MainNav.GetUnsignBuff(11))<<24);
        mTimerRunning = MainNav.mBuffer[12];*/
        mPrescaler = MainNav.GetUnsign16(2);
        mPeriod = MainNav.GetUnsign32(4);
        mPulse = MainNav.GetUnsign32(8);

    }

    @Override
    protected void CallWhenReceive(int nVal) {
        Log.d("phiLu", "SQUARE GEN : data received : " + nVal + " bytes ===\n");

        if ((MainNav.mBuffer[0]==1) && (MainNav.mBuffer[1]==10)) {
            GetTimerParam();
            mEdPrescaler.setText(String.valueOf(mPrescaler));
            mEdPeriod.setText(String.valueOf(mPeriod));
            mEdPulse.setText(String.valueOf(mPulse));
            Log.d("phiLu", "SQUARE GEN : timer param : " + mPrescaler + " "
                    + mPeriod + " " + mPulse + "\n");
        }
    }

    // set frequency
    public void SetFreq(View view) {

        // get parameters
        try {
            mPrescaler = Integer.parseInt(mEdPrescaler.getText().toString());
           if (mPrescaler<1) mPrescaler = 1;
           if (mPrescaler>0xffff)mPrescaler = 0xffff;

        } catch(NumberFormatException nfe) {
            mPrescaler=1000;
        }

        try {
            mPeriod = Long.parseLong(mEdPeriod.getText().toString());
            mPulse = Long.parseLong(mEdPulse.getText().toString());
            long max = 4294967295L; //0xffffffff;
            if (mPeriod<1) mPeriod=1;
            else if (mPeriod>max) mPeriod = max;
            if (mPulse<1) mPulse=1;
            else if (mPulse>mPeriod) mPulse = mPeriod/2;

        } catch(NumberFormatException nfe) {
            mPeriod=1000;
            mPulse=500;
        }

        // send new parameters
        MainNav.mOutBuffer[0]=10;  // square gen
        MainNav.mOutBuffer[1]=1;   // send param

        // prescaler
        MainNav.mOutBuffer[3] = (byte)(mPrescaler>>8);
        MainNav.mOutBuffer[2]=(byte) (mPrescaler-((short)MainNav.mOutBuffer[3]<<8));

        //period
        MainNav.Fill32outBuff(4, mPeriod);
        /*
        MainNav.mOutBuffer[7] = (byte)(mPeriod>>24);
        long i1 = mPeriod-(((long)MainNav.mOutBuffer[7])<<24);
        MainNav.mOutBuffer[6] = (byte)(i1>>16);
        long i2 = i1 - (((long)MainNav.mOutBuffer[6])<<16);
        MainNav.mOutBuffer[5] = (byte)(i2>>8);
        MainNav.mOutBuffer[4] = (byte) (i2 - (MainNav.mOutBuffer[5]<<8));
        */

        //pulse
        MainNav.Fill32outBuff(8, mPulse);
        /*
        MainNav.mOutBuffer[11] = (byte)(mPulse>>24);
        i1 = mPulse-(((long)MainNav.mOutBuffer[11])<<24);
        MainNav.mOutBuffer[10] = (byte)(i1>>16);
        i2 = i1 - (((long)MainNav.mOutBuffer[10])<<16);
        MainNav.mOutBuffer[9] = (byte)(i2>>8);
        MainNav.mOutBuffer[8] = (byte) (i2 - (MainNav.mOutBuffer[9]<<8));
        */
        SendData();
    }

    // start generator
    public void StartGen(View view) {
        if (mTimerRunning==0) {
            mTimerRunning = 1;
            MainNav.mOutBuffer[0] = 10;  // square gen
            MainNav.mOutBuffer[1] = 2;   // start
            SendData();
        }
    }

    // stop generator
    public void StopGen(View view) {
        if (mTimerRunning!=0) {
            mTimerRunning=0;
            MainNav.mOutBuffer[0] = 10;  // square gen
            MainNav.mOutBuffer[1] = 3;   // stop
            SendData();
        }
    }



}
