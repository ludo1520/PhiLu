package fr.ludo1520.philu;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class CalculFunction extends AppCompatActivity implements functListFragment.funcListListener,
        AdapterView.OnItemSelectedListener {

    public static final String PREFS_NAME = "PhiluPref";
    public static final String N_FUNC_NAME = "NumFunc";

    MathFuncParser mF = new MathFuncParser();
    EditText mEditFunc;
    Button mEvalButton;
    TextView mTextResult;
    Spinner mEditSpinner;

    private functListFragment mFuncList;
	String mExExpr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcul_func);


        mEditFunc = (EditText) findViewById(R.id.edit_function);
        mEditFunc.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE) Eval(null);
                return false;
            }
        });

        mEvalButton = (Button) findViewById(R.id.evalButton);
        mTextResult = (TextView) findViewById(R.id.viewCalcResult);

        // Spinner for inserting special characters :
        mEditSpinner = (Spinner) findViewById(R.id.edit_spinner);
        mEditSpinner.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.editChar_array, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mEditSpinner.setAdapter(spinnerAdapter);

        // get recorded function list
        mFuncList = new functListFragment();
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String str;
        int nFunc = settings.getInt(N_FUNC_NAME,0);
        for (int i=0; i<nFunc;++i) {
            str = "colorToDraw" + i;
            int colorToDraw = settings.getInt(str,0);
            str = "func" + i;
            mFuncList.AddToEnd(settings.getString(str,""),colorToDraw);
        }

        mExExpr="";

        //
        getFragmentManager().beginTransaction()
                .replace(R.id.containerCalcB, mFuncList).commit();
    }


    @Override
    protected void onStop() {
        super.onStop();

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        String str;

        int nFunction = mFuncList.Nfunc();
        editor.putInt(N_FUNC_NAME,nFunction);

        for (int i=0; i<nFunction;++i) {
            str = "func" + i;
            editor.putString(str,mFuncList.GetFuncStr(i));
            str = "colorToDraw"+i;
            editor.putInt(str, mFuncList.GetColorDraw(i));

        }
        //editor.apply();
        editor.commit(); // need to write now because another activity might need it
    }




    /***************************************************
     *  do something when user hits eval button
     *
     ***************************************************/

    public void Eval(View view) {
        String expr = mEditFunc.getText().toString();

        if (!expr.equals(mExExpr)) {

            mExExpr = expr;
            mF.Define(expr);
            if (mF.M_DefineError==0) {
                if (mF.FunctionOfX()) {
                    mFuncList.Add(expr,0);
                    //mTextResult.setText("");
                } else { // if not a function of x
                    String strResult = String.valueOf(mF.Eval(0));
                    if (mF.M_error==0) {
                        mFuncList.Add(expr,0);
                        mTextResult.setText(strResult);
                    } else {
                        mTextResult.setText("error");
                    }
                }
            } else { // if mF.M_DefineError !=0
                mTextResult.setText("error");
            }
        }

    }

    public void Suppr(View view) {

        //mEditFunc.clearComposingText(); marche pas

        Editable str = mEditFunc.getText();
        str.clear();
    }



        /***************************************************
         *  implements functListFragment.funcListListener
         *
         ***************************************************/

    @Override
    public void onFuncSelected(String exp, int position) {
        mEditFunc.setText(exp);
        mTextResult.setText("");
    }


    /*********************************************************
     * implements OnItemSelectedListener (spinner)
     *
     * Spinner for inserting special characters
     *
     ********************************************************/

    @Override
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        Editable str = mEditFunc.getText();
        CharSequence cs = (CharSequence) parent.getItemAtPosition(pos);
        int stopSelection = mEditFunc.getSelectionEnd();
        str.insert(stopSelection, cs);
        parent.setSelection(0);
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
    }

}
