package fr.ludo1520.philu;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class HistoActivity  extends AppCompatActivity {

    private HistoScreen mHistScreen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_histo_graph);

        // Fill mHistScreen
        mHistScreen = new HistoScreen();
        mHistScreen.mXmin = -5;
        mHistScreen.mXmax = 5;
        mHistScreen.mYmin = -5;
        mHistScreen.mYmax = 5;

        // get recorded function list
        SharedPreferences settings = getSharedPreferences(CalculFunction.PREFS_NAME, 0);
        String str;
        int nFunc = settings.getInt(CalculFunction.N_FUNC_NAME,0);
        for (int i=0; i<nFunc;++i) {
            str = "colorToDraw" + i;
            int colorToDraw = settings.getInt(str,0);
            if (colorToDraw!=0) {
                str = "func" + i;
                mHistScreen.AddFunction(settings.getString(str,""), colorToDraw);
            }
        }


        getFragmentManager().beginTransaction()
                .replace(R.id.histoScreen, mHistScreen).commit();
    }


}
