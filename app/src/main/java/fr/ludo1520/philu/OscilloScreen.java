package fr.ludo1520.philu;

import android.graphics.Canvas;
import android.graphics.Color;

/**
 *  implementation of GraphFragment that draw the oscilloscope screen
 */
public class OscilloScreen extends GraphFragment {

    public static float[] mDataBuff = new float[2048];
    public int nPoints = 0;

    @Override
    protected void Draw() {
        int w = mTextureView.getWidth();
        int h = mTextureView.getHeight();
        mScaleX = (float)w/(mXmax-mXmin);
        mScaleY = (float)h/(mYmin-mYmax);

        Canvas canvas = mTextureView.lockCanvas();
        canvas.drawColor(mBackColor);

        float dX = mXmin*mScaleX;
        float dY = mYmax*mScaleY;
        // Translation only for drawing axes and labels
        canvas.translate(-dX, -dY);

        // Draw axis
        if ((mYmin<0)&&(mYmax>0)) {
            canvas.drawLine(dX, 0, mXmax*mScaleX, 0, mPaint);
        }
        if ((mXmin<0)&&(mXmax>0)) {
            canvas.drawLine(0, dY, 0, mYmin*mScaleY, mPaint);
        }


        canvas.drawText("test text",50,50,mPaint);


        // Draw data points

        canvas.scale( mScaleX, mScaleY);

        float x1 = 0, x2;
        float y1 = mDataBuff[0], y2;
        float dx = 1;

        for (int i=1; i<nPoints; ++i) {
            x2 = x1+dx;
            y2 = mDataBuff[i];
            canvas.drawLine( x1,y1,x2,y2,mPaint);
            x1 = x2;
            y1 = y2;
        }


        mPaint.setColor(mPenColor);

        mTextureView.unlockCanvasAndPost(canvas);
    }



}
