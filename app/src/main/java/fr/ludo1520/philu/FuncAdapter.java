package fr.ludo1520.philu;


/*
Copyright 2014 Ludovic Gaudichet

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;


class ViewHolder {

    CheckBox checkBox; // to draw or not
    TextView functTV;
    TextView resultTV;
    Button colorSpace;
    int position;
}

/************************************************************
 *
 * class FuncAdapter
 *
 *
 ************************************************************/

public class FuncAdapter  extends BaseAdapter {

    // list of activities that listen to this adapter
    private ArrayList<FuncAdapterListener> mListListener = new ArrayList<FuncAdapterListener>();

    private List<String> mListStr;

    public static int Ncolor = 5;
    private int mColorList[] = new int[Ncolor];
    int mColorCompt;
    private int mListColorDraw[];
    // mListColorDraw has to be set !
    public void SetListColorDraw(int list[]) { mListColorDraw = list;}

    //public boolean mListSelected[] = new boolean[FunctList.NFuncMax]; // selection going with long click
    public boolean mListSelected[] = new boolean[200]; // selection going with long click

    private Activity mContext;
    private LayoutInflater mInflater;
    private MathFuncParser mFunc;

    public FuncAdapter(Context context, List<String> listStr) {
        mContext = (Activity) context;
        mListStr = listStr;
        mInflater = LayoutInflater.from(mContext);
        mFunc = new MathFuncParser();
        mColorList[0]=Color.BLUE;
        mColorList[1]=Color.RED;
        mColorList[2]=0xff00aa00; // dark green
        mColorList[3]=0xff00aaaa; // dark cyan
        mColorList[4]=Color.MAGENTA;
    }

    @Override
    public int getCount() {
        return mListStr.size();
    }
    @Override
    public Object getItem(int position) {
        return mListStr.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    void ClearSelection() {

        //for (int i=0; i<FunctList.NFuncMax;++i)
        for (int i=0; i<200; ++i)
            mListSelected[i] = false;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        RelativeLayout layoutItem;
        if (convertView == null) {
            layoutItem = (RelativeLayout) mInflater.inflate(R.layout.listfunc_item, parent, false);


            layoutItem.setOnClickListener( new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewHolder holder = (ViewHolder) v.getTag();
                    //telling the listeners that there has been a click on the TextView
                    sendListener(mListStr.get(holder.position), holder.position);
                }
            });

            // with CreateActionMode
            layoutItem.setOnLongClickListener( new OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    ViewHolder holder = (ViewHolder) v.getTag();
                    mListSelected[holder.position] = ! mListSelected[holder.position];
                    return false; // false to send the onLonClick to listview
                }
            });
            ViewHolder holder = new ViewHolder();
            holder.checkBox = (CheckBox)layoutItem.findViewById(R.id.checkbox_draw);
            holder.functTV = (TextView)layoutItem.findViewById(R.id.list_item_func);
            holder.resultTV = (TextView)layoutItem.findViewById(R.id.list_item_result);
            holder.colorSpace = (Button)layoutItem.findViewById(R.id.xfunct_color);
            holder.position = position;
            layoutItem.setTag(holder);

            holder.checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    ViewHolder hr = (ViewHolder)buttonView.getTag();
                    if (isChecked) {
                        if (mListColorDraw[hr.position]==0) {
                            mListColorDraw[hr.position]= GetNextColor();
                            hr.colorSpace.setBackgroundColor(mListColorDraw[hr.position]);
                        }
                    } else {
                        mListColorDraw[hr.position]=0;
                        hr.colorSpace.setBackgroundColor(mListColorDraw[hr.position]);
                    }
                }
            });

            holder.colorSpace.setOnClickListener( new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewHolder hr = (ViewHolder)v.getTag();
                    boolean updateCheckbox = (mListColorDraw[hr.position]==0);
                    mListColorDraw[hr.position]= GetNextColor();
                    v.setBackgroundColor(mListColorDraw[hr.position]);
                    if (updateCheckbox) {
                        hr.checkBox.setChecked(true);
                    }
                }
            });

        } else {
            layoutItem = (RelativeLayout) convertView;
        }

        ViewHolder myHolder = (ViewHolder) layoutItem.getTag();
        myHolder.checkBox.setTag(myHolder);
        myHolder.position = position;
        int colorDraw = mListColorDraw[position];
        myHolder.checkBox.setChecked(colorDraw!=0);
        myHolder.functTV.setTag(position);
        myHolder.colorSpace.setTag(myHolder);
        myHolder.colorSpace.setBackgroundColor(colorDraw);

        String exp = mListStr.get(position);
        myHolder.functTV.setText(exp);

        mFunc.Define(exp);
        if (mFunc.M_DefineError==0) {
            if (mFunc.FunctionOfX()) {
                myHolder.resultTV.setText("");
            } else { // if not a function of x
                String strResult = String.valueOf(mFunc.Eval(0));
                if (mFunc.M_error==0) {
                    myHolder.resultTV.setText(strResult);
                } else {
                    myHolder.resultTV.setText("error "+mFunc.M_error);
                }
            }
        } else { // if mFunc.M_DefineError !=0
            myHolder.resultTV.setText("error "+mFunc.M_error);
        }

        if (mListSelected[position])
            layoutItem.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_blue_bright));
        else layoutItem.setBackgroundColor(mContext.getResources().getColor(android.R.color.background_light));
        return layoutItem;
    }

    public int GetNextColor() {
        if (mColorCompt>=5) mColorCompt=0;
        return mColorList[mColorCompt++];
    }

    /**************************************************************
     *
     * listener : define what an activity must implement in order to
     * listen what is going on
     *
     **************************************************************/


    public interface FuncAdapterListener {
        public void onItemClick(String exp,int position);
    }

    private void sendListener(String exp, int position) {
        for(int i = mListListener.size()-1; i >= 0; i--) {
            mListListener.get(i).onItemClick(exp, position);
        }
    }

    public void addListener(FuncAdapterListener listener) {
        mListListener.add(listener);
    }

}