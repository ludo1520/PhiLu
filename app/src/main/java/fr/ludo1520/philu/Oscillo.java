package fr.ludo1520.philu;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.Spinner;

public class Oscillo extends BaseUSBlink {

    private int mADCmode;
    private boolean mADCisOn;
    private int mNChannelBits;
    private boolean mV1on;
    private boolean mV2on;
    private boolean mV3on;
    private int mNsample;


    private OscilloScreen mScreen;
    private Spinner mCommandSpinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oscillo);

        MainNav.mOutBuffer[0] = 1; // ask for information
        MainNav.mOutBuffer[1] = 20; // get oscilloscope parameters
        SendData();

        mCommandSpinner = (Spinner) findViewById(R.id.interactSpin);









        mScreen = new OscilloScreen();

        mScreen.mXmin = 0;
        mScreen.mXmax = 205;
        mScreen.mYmin = -2000;
        mScreen.mYmax = 5000;
        mScreen.mBackColor = Color.LTGRAY;

        getFragmentManager().beginTransaction()
			.replace(R.id.containerScreen, mScreen).commit();

    }


    @Override
    protected void CallWhenReceive(int nVal) {
        //Log.d("phiLu", "OSCILLO : data received : " + nVal + " bytes ===\n");

        switch (MainNav.GetUnsignBuff(0)) {

            case 1:{
                mADCmode = MainNav.GetUnsignBuff(1);
                mADCisOn = MainNav.mBuffer[2]==1;
                mV1on = (MainNav.mBuffer[3] & 0b00000001)==0b00000001;
                mV2on = (MainNav.mBuffer[3] & 0b00000010)==0b00000010;
                mV3on = (MainNav.mBuffer[3] & 0b00000100)==0b00000100;
                mNChannelBits = MainNav.mBuffer[3];
                mNsample = MainNav.GetUnsign16(4);
                if (!mADCisOn)
                switch (mADCmode) {
                    case 20: fireMode20(); break;
                    case 21: fireMode21(); break;
                }
                //Log.d("phiLu", "OSCILLO : ADCmode=" + mADCmode + " ADCisOn=" + mADCisOn
                //    + " Nsample=" + mNsample + "\n");
                //if (mV1on) Log.d("phiLu", "OSCILLO : V1 on\n");
                //if (mV2on) Log.d("phiLu", "OSCILLO : V2 on\n");
                //if (mV3on) Log.d("phiLu", "OSCILLO : V3 on\n");
            } break;

            case 20:{

                int maxNtrans = 30; // card send 30*2 bytes max per bunch
                //MainNav.mBuffer[1]; // not used
                int iTransfert = MainNav.GetUnsignBuff(2);
                //Log.d("phiLu", "OSCILLO : mode 20, iTRansfert ="+iTransfert+ "\n");

                if (iTransfert==255) { // last part

                    int shift = (mNsample/maxNtrans)*maxNtrans;
                    int nfill = mNsample-shift;
                    //Log.d("phiLu", "OSCILLO : last transfer shift="+shift+" nfill="+nfill+ "\n");
                    for (int i=0;i<nfill;++i) {
                        OscilloScreen.mDataBuff[shift+i] = MainNav.GetUnsign16(3+i*2);
                    }
                    mScreen.nPoints = mNsample;
                    mScreen.Draw();
                    fireMode20(); // start again

                } else {

                    int shift = (iTransfert-1)*maxNtrans;
                    for (int i=0;i<maxNtrans;++i) {
                        OscilloScreen.mDataBuff[shift+i] = MainNav.GetUnsign16(3+i*2);
                    }
                }


            } break;

        }
    }


    private void fireMode20() { // one channel 7.2 MHz
        MainNav.mOutBuffer[0] = 20;
        MainNav.mOutBuffer[1] = 1; //start
        //MainNav.mOutBuffer[2] = 0; // never used
        MainNav.mOutBuffer[3] = (byte)mNChannelBits; // set to 1 by card in mode==20
        MainNav.Fill16outBuff(4, mNsample);
        SendData();
    }


    private void fireMode21() { // 3 channels 2.4 MHz
        MainNav.mOutBuffer[0] = 21;
        MainNav.mOutBuffer[1] = 1; //start
        //MainNav.mOutBuffer[2] = 0; // never used
        MainNav.mOutBuffer[3] = (byte)mNChannelBits;
        MainNav.Fill16outBuff(4, mNsample);
        SendData();
    }




}