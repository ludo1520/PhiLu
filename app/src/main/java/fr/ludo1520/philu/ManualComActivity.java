package fr.ludo1520.philu;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ManualComActivity extends AppCompatActivity {


    //=================================================================================
    // receive USB detached
    //=================================================================================

    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            CallWhenUSBunplug();
            /*
            String action = intent.getAction();
            if (UsbManager.ACTION_USB_ACCESSORY_DETACHED.equals(action)) {
                CallWhenUSBunplug();
            } // end ACTION_USB_ACCESSORY_DETACHED
            */
        }
    };
    //=================================================================================

    private void CallWhenUSBunplug() {
        NavUtils.navigateUpFromSameTask(this);
    }

    private TextView mTV;
    private EditText mEditB1, mEditB2, mEditB3;
    private StringBuilder mLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_com);

        mTV = (TextView)findViewById(R.id.byteIn);
        mEditB1 = (EditText)findViewById(R.id.byteOut1);
        mEditB2 = (EditText)findViewById(R.id.byteOut2);
        mEditB3 = (EditText)findViewById(R.id.byteOut3);

        mLog = new StringBuilder();
    } // end onCreate


    private BroadcastReceiver ServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int nVal = intent.getIntExtra("nVal", 0);

            //log.append("=== data received : " + nVal + " bytes ===\n");

            mLog.insert(0,"\n");
            for (int i=nVal-1; i>=0; --i) {

                mLog.insert(0,MainNav.mBuffer[i]);
                if (MainNav.mBuffer[i]<10) mLog.insert(0,"   ");
                else if (MainNav.mBuffer[i]<100) mLog.insert(0,"  ");
                else mLog.insert(0," ");

                if (i%5==0) mLog.insert(0,"\n");
            }
            mLog.insert(0,"\n=== data received : " + nVal + " bytes ===\n");

            mTV.setText(mLog.toString());
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(MainNav.ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
        registerReceiver(mUsbReceiver, filter);

        IntentFilter servFilter = new IntentFilter(MainNav.UsbService.DataIn);
        LocalBroadcastManager.getInstance(this).registerReceiver(ServiceReceiver, servFilter);
    } //end onResume


    @Override
    public void onPause() {
    	super.onPause();

    	// Unregister the listeners when the application is paused
        LocalBroadcastManager.getInstance(this).unregisterReceiver(ServiceReceiver);
        unregisterReceiver(mUsbReceiver);
    } // end onPause


    // send bytes
    public void doSend(View view) {

        try {
            MainNav.mOutBuffer[0] = (byte) Integer.parseInt(mEditB1.getText().toString());

        } catch(NumberFormatException nfe) {
            MainNav.mOutBuffer[0]=0;
        }
        try {
            MainNav.mOutBuffer[1] = (byte) Integer.parseInt(mEditB2.getText().toString());

        } catch(NumberFormatException nfe) {
            MainNav.mOutBuffer[1]=0;
        }
        try {
            MainNav.mOutBuffer[3] = (byte) Integer.parseInt(mEditB3.getText().toString());

        } catch(NumberFormatException nfe) {
            MainNav.mOutBuffer[3]=0;
        }

    	Intent iout = new Intent(MainNav.UsbService.DataOut);
	    LocalBroadcastManager.getInstance(this).sendBroadcast(iout);
    }



} // end class ManualComActivity
