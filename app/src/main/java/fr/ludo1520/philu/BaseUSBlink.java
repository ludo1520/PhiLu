package fr.ludo1520.philu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbManager;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;



public class BaseUSBlink extends AppCompatActivity {


    //=================================================================================
    // to send data
    //=================================================================================

    Intent mIntentOut = new Intent(MainNav.UsbService.DataOut);

    public void SendData() {
        LocalBroadcastManager.getInstance(this).sendBroadcast(mIntentOut);
    }


    //=================================================================================
    // receive USB detached
    //=================================================================================

    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            CallWhenUSBunplug();
            /*
            String action = intent.getAction();
            if (UsbManager.ACTION_USB_ACCESSORY_DETACHED.equals(action)) {
                CallWhenUSBunplug();
            } // end ACTION_USB_ACCESSORY_DETACHED
            */
        }
    };

    private void CallWhenUSBunplug() {
        NavUtils.navigateUpFromSameTask(this);

    }


    //=================================================================================
    // receiving USB data
    //=================================================================================

    private BroadcastReceiver ServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int nVal = intent.getIntExtra("nVal", 0);
            CallWhenReceive(nVal);
        }
    };

    protected void CallWhenReceive(int nVal) {
    }
    //=================================================================================

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(MainNav.ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
        registerReceiver(mUsbReceiver, filter);
        IntentFilter servFilter = new IntentFilter(MainNav.UsbService.DataIn);
        LocalBroadcastManager.getInstance(this).registerReceiver(ServiceReceiver, servFilter);
    } //end onResume

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(ServiceReceiver);
        unregisterReceiver(mUsbReceiver);
    } // end onPause


} // end class BaseUSBlink
