package fr.ludo1520.philu;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.os.Environment;
import android.app.Fragment;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;


public class GraphFragment extends Fragment
        implements TextureView.SurfaceTextureListener, GestureDetector.OnGestureListener {

	public int mBackColor = Color.WHITE;
    public int mPenColor = Color.BLACK;
    protected float mLabelSize = 30;

    public float mXmin, mXmax, mYmin, mYmax;
    protected float mScaleX, mScaleY;

    protected graduation mGrX1 = new graduation();
    protected graduation mGrY1 = new graduation();

    protected TextureView mTextureView;
    protected GestureDetector mDetector;
    protected ScaleGestureDetector mScaleDetector;
    protected Paint mPaint;

    /************************************************************
     *
     * methods Android
     *
     *
     ************************************************************/

    public GraphFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_graph, container, false);
    }


    @Override
    public void onActivityCreated (Bundle savedInstanceState) {

        mDetector = new GestureDetector(getActivity(),this);
        mScaleDetector = new ScaleGestureDetector(getActivity(), new ScaleListener());
        mTextureView = (TextureView) getView().findViewById(R.id.fragGraph);
        mTextureView.setSurfaceTextureListener(this);
        mTextureView.setOpaque(false);
        mTextureView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                // Respond to touch events
                mDetector.onTouchEvent(event);
                mScaleDetector.onTouchEvent(event);
                return true;
            }
        });

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setTextSize(mLabelSize);
        mPaint.setTextAlign(Paint.Align.CENTER);

        super.onActivityCreated(savedInstanceState);
    }


    /************************************************************
     *
     * Draw
     *
     *
     ************************************************************/

    protected void Draw() {

        int w = mTextureView.getWidth();
        int h = mTextureView.getHeight();
        mScaleX = (float)w/(mXmax-mXmin);
        mScaleY = (float)h/(mYmax-mYmin);

        Canvas canvas = mTextureView.lockCanvas();
        canvas.drawColor(mBackColor);

        float dX = mXmin*mScaleX;
        float dY = mYmin*mScaleY;
        // Pour les axes, on ne fait que la translation
        canvas.translate(-dX, -dY);

        // Draw axis
        if ((mYmin<0)&&(mYmax>0)) {
            canvas.drawLine(dX, 0, mXmax*mScaleX, 0, mPaint);
        }
        if ((mXmin<0)&&(mXmax>0)) {
            canvas.drawLine(0, dY, 0, mYmax*mScaleY, mPaint);
        }

        /*
        // Draw the axis graduation and their labels
        float gradLength = 10;

        // x axis
        float xlabelDrawY = mLabelSize+gradLength;
        float xAxisDrawY = 0;
        if (mYmin>0) {
            xAxisDrawY = dY;
            xlabelDrawY += xAxisDrawY;
        }
        if (mYmax<0) {
            xAxisDrawY = mYmax*mScaleY;
            xlabelDrawY = xAxisDrawY-mLabelSize/2-gradLength;
        }

        float posScaled;
        int i;
        mGr.Set(mXmin,mXmax ,w, mLabelSize, true);
        for (i=0; i<mGr.numGrad;++i) {
            mGr.SetPos(i);
            posScaled = mGr.currentPos*mScaleX;
            canvas.drawLine( posScaled,xAxisDrawY-gradLength,
                    posScaled, xAxisDrawY+gradLength, mPaint);
            canvas.drawText(mGr.str,
                    posScaled,xlabelDrawY,mPaint);
        }

        // y axis
        mGr.Set(mYmin,mYmax ,h, mLabelSize, false);

        float ylabelDraw = -gradLength-mLabelSize*mGr.str.length()/2.5f;
        float yAxisDrawX = 0;

        if (mXmin>0) {
            yAxisDrawX = dX;
            ylabelDraw =  yAxisDrawX-ylabelDraw;
        }
        if (mXmax<0) {
            yAxisDrawX = mXmax*mScaleX;
            ylabelDraw += yAxisDrawX;
        }


        for (i=0; i<mGr.numGrad;++i) {
            mGr.SetPos(i);
            posScaled = mGr.currentPos*mScaleY;
            canvas.drawLine( yAxisDrawX-gradLength, posScaled,
                    yAxisDrawX+gradLength, posScaled,mPaint);
            canvas.drawText(mGr.str, ylabelDraw,posScaled,mPaint);
        }

        // Draw functions of x
        canvas.scale( mScaleX, -mScaleY);
        int nPt = w/2;
        float dx = (mXmax-mXmin)/nPt;
        float x2,y2;

        //paint.setStrokeWidth(2);
        for (i=mFunc.size()-1; i>=0; --i) {
            MathFuncParser f = mFunc.get(i);
            mPaint.setColor(f.mColor);
            float x1 = mXmin;
            float y1 = (float) f.Eval(x1);
            int erreur1 = f.M_error;
            for (int j=0; j<nPt; ++j) {
                x2 = x1+dx;
                y2 =  (float) f.Eval(x2);
                if ((f.M_error==0)&&(erreur1==0)) canvas.drawLine( x1,y1,x2,y2,mPaint);
                x1 = x2;
                y1 = y2;
                erreur1 = f.M_error;
            }
        }
        */

        mPaint.setColor(Color.BLACK);

        mTextureView.unlockCanvasAndPost(canvas);
    }




    /************************************************************
     *
     * implementing TextureView.SurfaceTextureListener
     *
     *
     ************************************************************/

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface,
                                          int width, int height) {
        Draw();
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width,
                                            int height) {
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    }



    /************************************************************
     *
     * implementing GestureDetector.OnGestureListener
     * for scrolling
     *
     ************************************************************/

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                           float velocityY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                            float distanceY) {

        mXmin += distanceX/mScaleX;
        mXmax += distanceX/mScaleX;
        mYmin += distanceY/mScaleY;
        mYmax += distanceY/mScaleY;
        Draw();
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }


    /************************************************************
     *
     * ScaleListener
     * for scaling
     *
     ************************************************************/

    private class ScaleListener
            extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {

            // Don't let the object get too small or too large.
            //getFocusX()
            if (detector.isInProgress()) {
                float previousSpan = detector.getPreviousSpanX();
                if (previousSpan>30) {
                    float dX = mXmax-mXmin;
                    float zoom = previousSpan/detector.getCurrentSpanX();
                    mXmin += (1f-zoom)*detector.getFocusX()/mScaleX;
                    mXmax = mXmin + dX*zoom;
                    //mXmin /= zoom;
                    //mXmax /= zoom;
                }
                previousSpan = detector.getPreviousSpanY();
                if (previousSpan>30) {
                    float dY = mYmax-mYmin;
                    float zoom = previousSpan/detector.getCurrentSpanY();
                    mYmin -= (1f-zoom)*detector.getFocusY()/mScaleY;
                    mYmax = mYmin + dY*zoom;
                    //mYmin /= zoom;
                    //mYmax /= zoom;
                }
            }

            Draw();

            return true;
        }
    }



    /************************************************************
     *
     * Save to PNG
     *
     ************************************************************/

    public File savePNG() {

        boolean didit = true;
        Bitmap bmp = mTextureView.getBitmap();

        File  mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "PhiLu");
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        //File cache = getActivity().getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File sharefile = new File(mediaStorageDir,"capture.png");

        try {
            FileOutputStream out = new FileOutputStream(sharefile);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
        } catch (IOException e) {
            didit = false;
        }
        if (didit) return sharefile; else return null;
    }

}


/************************************************************
 *
 * Pour mettre les graduations
 *
 *
 ************************************************************/


class graduation {

    private boolean mXaxis;
    private float mMin, mTextSize;
    private double order, pow10;

    public double mRange,distGrad,firstGrad;
    public int m, numGrad;
    public float strPix;
    DecimalFormat formatter = new DecimalFormat();

    public String str;
    public float currentPos;

    public void SetPos(int i) {
        currentPos = (float) (firstGrad + distGrad * i);
        //if (mXaxis) {
        str = GetStr(currentPos);
        //} else { str = GetStr(-currentPos); }
    }


    //==============================================
    public String GetStr(double val) {
        return formatter.format(val);
    }

    //==============================================
    private void SetGrad(float div) {

        distGrad = div*pow10;
        firstGrad = Math.floor(mMin/distGrad);
        //if (mMin<0) firstGrad = (firstGrad+1)*distGrad; // car floor(-1.1) = -2 et non -1
        firstGrad = (firstGrad+1)*distGrad; // car floor(-1.1) = -2 et non -1 ?
        //else firstGrad *=distGrad;

        str = GetStr(firstGrad);
        if (mXaxis) {
            strPix = str.length()*mTextSize;
        }
        else {
            strPix = 2*mTextSize;
        }
        numGrad = (int)(mRange/distGrad)+1;
    }

    //==============================================
    public void Set(float min, float max, int npixScreen, float textSize, boolean xaxis) {

        mXaxis = xaxis;
        mMin = min;
        mTextSize = textSize;
        mRange = max-min;
        order = Math.floor(Math.log10(mRange));
        pow10 = Math.pow(10,order);
        m = (int)(mRange/pow10);
        // -> range = m.xxxx * 10^order

        //System.out.println("range = " + mRange + " m = " + m + " order = " + order );

        if ((min>-1000f)&&(max<1000f)&&(mRange>0.001))
            formatter.applyPattern("##0.###");
        else
            formatter.applyPattern("0.######E0");

        SetGrad(0.1f);
        if (numGrad*strPix>npixScreen) SetGrad(0.2f);
        if (numGrad*strPix>npixScreen) SetGrad(0.5f);
        if (numGrad*strPix>npixScreen) SetGrad(1f);
    } // end constructor

    //pos = (Math.round(mXmin/dxGraduation)+i)*dxGraduation;

} // end class "graduation"

