package fr.ludo1520.philu;


// USB accessory :
// https://github.com/YuuichiAkagawa/HelloADK
// https://github.com/YuuichiAkagawa/HelloADK/blob/master/src/org/ammlab/android/helloadk/HelloADKActivity.java



import android.app.IntentService;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;


import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;



public class MainNav extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


	public static final String ACTION_USB_PERMISSION ="fr.ludo1520.philu.action.USB_PERMISSION";
	static UsbManager mUsbManager;
	PendingIntent mPermissionIntent;

    //=================================================================================
    // Data
    //=================================================================================
    public static byte[] mBuffer = new byte[64];
    public static byte[] mOutBuffer = new byte[12];

    public static int GetUnsignBuff(int i) {
        return mBuffer[i] & 0xff;
    }

    public static char GetUnsign16(int i) {
        return (char)((mBuffer[i] & 0xff) + ((mBuffer[i+1] & 0xff) << 8));
    }
    public static long GetUnsign32(int i) {
        return (mBuffer[i] & 0xff) + ((mBuffer[i+1] & 0xff) << 8) +
        ((mBuffer[i+2] & 0xff) << 16) + ((long)(mBuffer[i+3] & 0xff) << 24);
    }


    public static void Fill32outBuff(int pos, long in) {

        mOutBuffer[pos+3] = (byte)(in>>24);
        long i1 = in - (((long)mOutBuffer[pos+3])<<24);
        mOutBuffer[pos+2] = (byte)(i1>>16);
        long i2 = i1 - (((long)mOutBuffer[pos+2])<<16);
        mOutBuffer[pos+1] = (byte)(i2>>8);
        mOutBuffer[pos] = (byte) (i2 - (mOutBuffer[pos+1]<<8));
    }

    public static void Fill16outBuff(int pos, int in) {

        mOutBuffer[pos+1] = (byte)(in>>8);
        mOutBuffer[pos]=(byte) (in-((int)mOutBuffer[pos+1]<<8));
    }


    //=================================================================================
    // Service to handle USB data
    // allows to keep having it after onPause / on destroy / onCreate...
    // for service see : https://guides.codepath.com/android/Starting-Background-Services
    //=================================================================================


    public static class UsbService extends IntentService {

        public static final String DataIn = "fr.ludo1520.philu.UsbService.Din";
        public static final String DataOut = "fr.ludo1520.philu.UsbService.Dout";
        public static boolean mRunning;

        private static ParcelFileDescriptor mFileDescriptor;
        private FileInputStream mUsbInputStream = null;
        private FileOutputStream mUsbOutputStream = null;

        // Must create a default constructor
        public UsbService() {
            // Used to name the worker thread, important only for debugging.
            super("UsbService");
        }

        // Define the callback when someone send and intent(DataOut)
        private BroadcastReceiver ownReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //int nVal = intent.getIntExtra("nValOut", 0);
                if (mUsbOutputStream != null) {
                    try {
                        mUsbOutputStream.write(mOutBuffer);
                    } catch (IOException e) {
                        Log.e("phiLu", " phiLuUSB service write failed", e);
                    }
                }
            }
        };

        @Override
        public void onCreate() {
            super.onCreate(); // if you override onCreate(), make sure to call super().
            // If a Context object is needed, call getApplicationContext() here.

            // willing to send data ->fill mOutBuffer and send an intent(DataOut) :
            IntentFilter filter = new IntentFilter(DataOut);
            LocalBroadcastManager.getInstance(this).registerReceiver(ownReceiver, filter);
        }


        @Override
        protected void onHandleIntent(Intent intent) {
            // This describes what will happen when service is triggered
            Log.d("phiLu", "Service onHandleIntent START");

            mRunning = true;
            UsbAccessory[] accessories = mUsbManager.getAccessoryList();
            UsbAccessory accessory = (accessories == null ? null : accessories[0]);
            if (accessory == null) return;

            mFileDescriptor = mUsbManager.openAccessory(accessory);
            if (mFileDescriptor == null) return;

            FileDescriptor fd = mFileDescriptor.getFileDescriptor();
            mUsbInputStream = new FileInputStream(fd);
            mUsbOutputStream = new FileOutputStream(fd);

            int ret = 0;
            while ((mRunning)&&(ret >= 0)) {
                try {
                    ret = mUsbInputStream.read(mBuffer);
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }
                if( ret > 0 ){
                    Intent in = new Intent(DataIn);
                    in.putExtra("nVal", ret);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(in);
                }
            } // end while

            Log.d("phiLu", "Service onHandleIntent STOP");
            try {
                if (mFileDescriptor != null) {
                    mFileDescriptor.close();
                }
            } catch (IOException e) {
            } finally {
                mFileDescriptor = null;
            }
            mRunning = false;
        } // end onHandleIntent


    }

    // Define the callback for what to do when data is received
    // It has to be registered by :
    // IntentFilter servFilter = new IntentFilter(UsbService.DataIn);
    // LocalBroadcastManager.getInstance(this).registerReceiver(ServiceReceiver, servFilter);
    //
    private BroadcastReceiver ServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int nVal = intent.getIntExtra("nVal", 0);
            Log.d("phiLu", "ServiceReceive onReceive: nVal = "+ nVal + " mBuffer[0]=" + mBuffer[0]+" mBuffer[1]="+mBuffer[1]);
        }
    };



    //=================================================================================
    // USB accessory
    // receive detached, other action won't come (?)
    //=================================================================================

    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            Log.d("phiLu", "### BroadcastReceiver on Receive");

            if (ACTION_USB_PERMISSION.equals(action)) {

                Log.d("phiLu", "### BroadcastReceiver action=usb_permission");

            } // end ACTION_USB_PERMISSION

            if (UsbManager.ACTION_USB_ACCESSORY_DETACHED.equals(action)) {
                Log.d("### BroadcastReceiver", "### action = DETACHED");

                //closeAccessory();

            } // end ACTION_USB_ACCESSORY_DETACHED

            if (UsbManager.ACTION_USB_ACCESSORY_ATTACHED.equals(action)) {
                Log.d("phiLu", "### BroadcastReceiver action = ATTACHED");
            }
        }
    };




























    //=================
    // afficher l'heure
    //=================
    /*
    private TextView mTVtime;
    BroadcastReceiver _TimeBReceiver;
    private final SimpleDateFormat _TimeFormat = new SimpleDateFormat("HH:mm");
    @Override
    public void onStart() {
        super.onStart();
        _TimeBReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context ctx, Intent intent) {
                if (intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0)
                    mTVtime.setText(_TimeFormat.format(new Date()));
            }
        };
        registerReceiver(_TimeBReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
    }
    @Override
    public void onStop() {
        super.onStop();
        if (_TimeBReceiver != null)
        unregisterReceiver(_TimeBReceiver);
    }
    */



    //=================================================================================
    // MainNav
    //
    //=================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_nav);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //mTVtime = (TextView) findViewById(R.id.timeView);

        Log.d("phiLu", "onCreate()");
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
	    mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
    }


    @Override
    public void onResume() {
        super.onResume();

        Log.d("phiLu", "onResume()");
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
		//filter.addAction(ACTION_USB_PERMISSION); // marche pas ?!
		filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
		registerReceiver(mUsbReceiver, filter);

        IntentFilter servFilter = new IntentFilter(UsbService.DataIn);
        LocalBroadcastManager.getInstance(this).registerReceiver(ServiceReceiver, servFilter);

        UsbAccessory[] accessories = mUsbManager.getAccessoryList();
    	UsbAccessory accessory = (accessories == null ? null : accessories[0]);
    	if (accessory != null)
    		if ((mUsbManager.hasPermission(accessory))&&(! UsbService.mRunning)) {

    				Log.d("phiLu", "### onResume : starting usb service !!!");
    				Intent inte = new Intent(this, UsbService.class);
    				startService(inte);
    		}

    } // end onResume


    @Override
    public void onPause() {
    	super.onPause();
    	Log.d("phiLu", "onPause()");

    	// Unregister the listeners when the application is paused
        LocalBroadcastManager.getInstance(this).unregisterReceiver(ServiceReceiver);
    	unregisterReceiver(mUsbReceiver);
    } // end onPause




    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_nav, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_calculs_function) {
            Intent intent = new Intent(this, CalculFunction.class);
            startActivity(intent);

        } else if (id == R.id.nav_histo) {
            Intent intent = new Intent(this, HistoActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_oscillo) {
            Intent intent = new Intent(this, Oscillo.class);
            startActivity(intent);

        } else if (id == R.id.nav_square_gen) {
            Intent intent = new Intent(this, SquareGen.class);
            startActivity(intent);

        } else if (id == R.id.nav_manualCom) {
            Intent intent = new Intent(this, ManualComActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_log) {
            Intent intent = new Intent(this, LogActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
