package fr.ludo1520.philu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

        TextView tv = (TextView)findViewById(R.id.messageLog);
        tv.setMovementMethod(new ScrollingMovementMethod());

        try {
            Process process = Runtime.getRuntime().exec("logcat -d phiLu:D *:S");
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            StringBuilder log=new StringBuilder();
            String line = "";
            String retour="\n";
            while ((line = bufferedReader.readLine()) != null) {
                //log.append(line);
                //log.append(retour);
                log.insert(0,line);
                log.insert(0,retour);
            }

            tv.setText(log.toString());
        }
        catch (IOException e) {}

    }
}
