package fr.ludo1520.philu;


import android.graphics.Canvas;

import java.util.ArrayList;

/**
 *  implementation of GraphFragment that draw functions and histograms
 */


public class HistoScreen extends GraphFragment {

    private ArrayList<MathFuncParser> mFunc = new ArrayList<MathFuncParser>();

    public void AddFunction(String str, int color) {
    	MathFuncParser f = new MathFuncParser();
    	f.mColor = color;
    	f.Define(str);
    	mFunc.add(f);
    }

    public void ClearFunc(){
    	mFunc.clear();
    }



    @Override
    protected void Draw() {
        int w = mTextureView.getWidth();
        int h = mTextureView.getHeight();
        mScaleX = (float) w / (mXmax - mXmin);
        mScaleY = (float) h / (mYmin - mYmax);

        Canvas canvas = mTextureView.lockCanvas();
        canvas.drawColor(mBackColor);


        float dX = mXmin*mScaleX;
        float dY = mYmax*mScaleY;
        // Translation only for drawing axes and labels
        canvas.translate(-dX, -dY);


        // Draw axis and graduation
        float x1,x2,y1,y2;
        int i;
        float gradLen = 0.01f*(w+h);
        mGrX1.Set(mXmin, mXmax ,w, mLabelSize, true);
        mGrY1.Set(mYmin, mYmax ,h, mLabelSize, false);

        // X axis
        if (mYmin<0) {
            if (mYmax>0) { // X axis is visible
                canvas.drawLine(dX, 0, mXmax*mScaleX, 0, mPaint);
                y1 = -gradLen;
                y2 =  gradLen;
            } else { // X axis not visible, showing graduations only
                y1 = mYmax*mScaleY;
                y2 = y1+gradLen;
            }
        } else {
            y1 = mYmin*mScaleY;
            y2 = y1-gradLen;
        }

        for (i=0; i<mGrX1.numGrad; ++i) {
                mGrX1.SetPos(i);
                x1 = mGrX1.currentPos*mScaleX;
                canvas.drawLine( x1,y1,x1,y2, mPaint);
                canvas.drawText(mGrX1.str, x1,gradLen*4,mPaint);
        }

        // Y axis
        if (mXmin<0) {
            if (mXmax>0) { // Y axis is visible
                x1 = 0;
                canvas.drawLine(x1, dY, x1, mYmin*mScaleY, mPaint);
                x2 = x1+gradLen;
                x1 = -gradLen;
            } else { // not visible
                x1 = mXmax*mScaleX;
                x2 = x1-gradLen;
            }
        } else {
            x1 = mXmin*mScaleX;
            x2 = x1+gradLen;
        }

        for (i=0; i<mGrY1.numGrad; ++i) {
            mGrY1.SetPos(i);
            y1 = mGrY1.currentPos*mScaleY;
            canvas.drawLine( x1,y1,x2,y1, mPaint);
            canvas.drawText(mGrY1.str, -gradLen*(2 + 0.5f*mGrY1.str.length()), y1,mPaint);
        }


        canvas.scale( mScaleX, mScaleY);
        // Draw data points
        //canvas.drawLine( x1,y1,x2,y2,mPaint);
        //canvas.drawLine( 1,1,2,2,mPaint);

        // Draw functions
        int nPt = w/3;
        float dx = (mXmax-mXmin)/nPt;

        //paint.setStrokeWidth(2);
        for (i=mFunc.size()-1; i>=0; --i) {
        	MathFuncParser f = mFunc.get(i);
        	mPaint.setColor(f.mColor);
        	x1 = mXmin;
        	y1 = (float) f.Eval(x1);
        	int erreur1 = f.M_error;
        	for (int j=0; j<nPt; ++j) {
        		x2 = x1+dx;
        		y2 =  (float) f.Eval(x2);
        		if ((f.M_error==0)&&(erreur1==0)) canvas.drawLine( x1,y1,x2,y2,mPaint);
        		x1 = x2;
        		y1 = y2;
        		erreur1 = f.M_error;
        	}
        }

        mPaint.setColor(mPenColor);
        mTextureView.unlockCanvasAndPost(canvas);
    }



}
